# CRST project

## Start the applications

Run `npx nx serve frontend` to start the frontend.
Run `npx nx serve backend` to start the backend.

## Build for production

Run `npx nx build frontend` to build the FE application.
Run `npx nx build backend` to build the BE application.
