import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';
import { appRoutes } from './app.routes';
import { AuthStore } from './services/auth.store';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideHttpClient } from '@angular/common/http';
import { LocationStore } from './services/location.store';

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(appRoutes), AuthStore, provideAnimations(), provideHttpClient(), LocationStore],
};
