import { patchState, signalStore, withComputed, withMethods, withState } from '@ngrx/signals';
import { User } from '../models/user';
import { computed } from '@angular/core';
export interface AuthState {
    user: User | null;
}

export const AuthStore = signalStore(
    withState<AuthState>({
      user: null,
    }),
    withComputed(({ user }) => ({
        isLoggedIn: computed(() =>
          !!user()),
        isLoggedOut: computed(() =>
          !user()),
      })),
    withMethods((store) => ({
      loginUser(user: User): void {
        patchState(store, {
          user
        });
      },
      logoutUser(): void {
        patchState(store, {
          user: null
        });
      }
    }))
  );
  