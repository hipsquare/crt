import { Injectable, inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Location } from "../models/location";
import { environment } from '../../environment/environment';

@Injectable({providedIn: 'root'})
export class ApiService{
    private _http = inject(HttpClient);

    getLocations(): Observable<Location[]>{
       return this._http.get<Location[]>(environment.API_URL + "locations");
    }
}
