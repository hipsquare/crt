import { patchState, signalStore, withMethods, withState } from '@ngrx/signals';
import { Location } from '../models/location';

export interface LocationState {
    locations: Location[];
}

export const LocationStore = signalStore(
    withState<LocationState>({
      locations: [],
    }),
    withMethods((store) => ({
      setLocations(locations: Location[]): void {
        patchState(store, {
          locations
        });
      }
    }))
  );
