import { Component, inject} from '@angular/core';
import { LoginComponent } from '../login/login.component';
import { NxHeaderModule } from '@aposin/ng-aquila/header';
import { NxLinkModule } from '@aposin/ng-aquila/link';
import { NxContextMenuModule } from '@aposin/ng-aquila/context-menu';
import { NxSidebarModule } from '@aposin/ng-aquila/sidebar';
import { NxIconModule } from '@aposin/ng-aquila/icon';
import { Router, RouterModule } from '@angular/router';
import { AuthStore } from '../../services/auth.store';
import { AuthService } from '../../services/auth.service';
import { NdbxIconModule } from '@allianz/ngx-ndbx/icon';
import { NxButtonModule } from '@aposin/ng-aquila/button';
import { environment } from '../../../environment/environment';

@Component({
  standalone: true,
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
  imports: [RouterModule, LoginComponent,  NxHeaderModule, NxLinkModule, NxContextMenuModule, NxSidebarModule, NxIconModule, NdbxIconModule, NxButtonModule],
})
export class HomeComponent {
  authStore = inject(AuthStore);
  authService = inject(AuthService);
  router = inject(Router);

  userEmail = this.authStore.user()?.email;

  logout(){
      this.authStore.logoutUser();
      window.open(environment.API_URL + "logout", "_self");
  }
}
