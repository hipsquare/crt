import { Component } from '@angular/core';
@Component({
  standalone: true,
  selector: 'app-resilience-services',
  templateUrl: './resilience-services.component.html',
  styleUrl: './resilience-services.component.scss',
})
export class ResilienceServicesComponent {}
