import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ResilienceServicesComponent } from './resilience-services.component';

describe('PerilsComponent', () => {
  let component: ResilienceServicesComponent;
  let fixture: ComponentFixture<ResilienceServicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ResilienceServicesComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ResilienceServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

    it('should create the resilience services component', () => {
    expect(component).toBeTruthy();
  });
});
