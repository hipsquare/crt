import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PerilsComponent } from './perils.component';

describe('PerilsComponent', () => {
  let component: PerilsComponent;
  let fixture: ComponentFixture<PerilsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PerilsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PerilsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

    it('should create the perils component', () => {
    expect(component).toBeTruthy();
  });
});