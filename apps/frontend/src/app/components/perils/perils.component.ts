import { Component } from '@angular/core';

@Component({
  standalone: true,
  selector: 'app-perils',
  templateUrl: './perils.component.html',
  styleUrl: './perils.component.scss',
})
export class PerilsComponent {}
