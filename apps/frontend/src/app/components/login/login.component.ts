import { Component, inject } from '@angular/core';
import { NxHeadlineModule } from '@aposin/ng-aquila/headline';
import { NxGridModule } from '@aposin/ng-aquila/grid';
import { NxInputModule } from '@aposin/ng-aquila/input';
import { NxButtonModule } from '@aposin/ng-aquila/button';
import { NxMessageModule } from '@aposin/ng-aquila/message';
import { AuthService } from '../../services/auth.service';
import { environment } from '../../../environment/environment';
@Component({
  standalone: true,
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
  imports: [
    NxHeadlineModule,
    NxGridModule,
    NxInputModule,
    NxButtonModule,
    NxMessageModule,],
    providers: [AuthService]
})
export class LoginComponent {

  authService = inject(AuthService);

  login(){
    window.open(environment.API_URL + "auth0/login", "_self");
  }

}
