import { Component, inject } from '@angular/core';
import { LocationStore } from '../../services/location.store';
import { ApiService } from '../../services/api.service';
import { LocationComponent } from './location/location.component';
@Component({
  standalone: true,
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrl: './locations.component.scss',
  imports: [LocationComponent]
})
export class LocationsComponent {
  locationStore = inject(LocationStore);
  apiService = inject(ApiService);

  constructor(){
      if(this.locationStore.locations().length === 0){
        this.apiService.getLocations().subscribe({
          next: x =>  this.locationStore.setLocations(x),
          error: err => console.log('An error occurred when loading locations :', err),
        });
    }
  }
}
