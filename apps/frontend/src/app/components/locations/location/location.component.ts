import { Component, Input } from '@angular/core';
import { Location } from '../../../models/location';
@Component({
  standalone: true,
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrl: './location.component.scss',
})
export class LocationComponent {
  @Input() location: Location = { id: '', name: '', address: '', latitude: 0, longitude: 0 };
}
