import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LocationsComponent } from './locations.component';
import { LocationStore } from '../../services/location.store';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('LocationsComponent', () => {
  let component: LocationsComponent;
  let fixture: ComponentFixture<LocationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LocationsComponent],
      providers: [LocationStore, HttpClient, HttpHandler]
    }).compileComponents();

    fixture = TestBed.createComponent(LocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

    it('should create the locations component', () => {
    expect(component).toBeTruthy();
  });
});
