import { Injectable, inject } from "@angular/core";
import { Router, UrlTree } from "@angular/router";
import { Observable, map } from "rxjs";
import { AuthStore } from "../services/auth.store";
import { AuthService } from "../services/auth.service";

@Injectable({providedIn: 'root'})
export class AuthGuard {

    authStore = inject(AuthStore);
    authService = inject(AuthService);
    router = inject(Router);

    canActivate(): boolean | Promise<boolean | UrlTree> | Observable<boolean | UrlTree> | UrlTree | undefined{
        if (this.authStore.isLoggedIn()) {
            return true;
        } else {
            return this.authService.getLoggedInUser().pipe(
                map((user)=>{
                    if(!!user && user.email !== "undefined"){
                        this.authStore.loginUser(user);
                        return true;
                    } else {
                        return this.router.createUrlTree(['/login']) 
                    }
                }
            ))        
        }
    }
}

