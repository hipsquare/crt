import { Route } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PerilsComponent } from './components/perils/perils.component';
import { LocationsComponent } from './components/locations/locations.component';
import { ResilienceServicesComponent } from './components/resilience-services/resilience-services.component';

export const appRoutes: Route[] = [
    { path: '', redirectTo: '/home', pathMatch: 'full'},
    {
        path: 'home',
        canActivate: [AuthGuard],
        component: HomeComponent,
        children: [
            {
              path: '',
              pathMatch: 'full',
              redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'perils',
                component: PerilsComponent
            },
            {
                path: 'locations',
                component: LocationsComponent
            },
            {
                path: 'resilience-services',
                component: ResilienceServicesComponent
            },
        ]
    },
    {
        path: 'login', 
        component: LoginComponent
    }

];
