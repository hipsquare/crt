import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { LocationService } from './location.service';
import { Location, CreateLocation } from './location';

@Controller('locations')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  @Get()
  getLocations(): Promise<Location[]> {
    const locations = this.locationService.getAll();
    return locations;
  }

  @Get(':id')
  getLocation(@Param('id') id: string): Promise<Location> {
    return this.locationService.getOne(id);
  }

  @Post()
  createLocation(@Body() newLocation: CreateLocation): Promise<Location> {
    return this.locationService.create(newLocation);
  }

  @Put(':id')
  update(@Body() updatedLocation: Location, @Param('id') id: string): Promise<Location> {
    return this.locationService.update(id, updatedLocation);
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.locationService.delete(id);
  }
}
