import { Injectable } from '@nestjs/common';
import { CreateLocation, Location } from './location';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class LocationService {
  constructor(
    @InjectRepository(Location) private locationRepository: Repository<Location>,
  ) {}

  getAll(): Promise<Location[]> {
    return this.locationRepository.find();
  }

  getOne(id: string): Promise<Location> {
    /* findOneById is deprecated however recommended findOneBy({id}) is not
    returning the correct result with the string id coming from MongoDb
    (TODO: get rid of the deprecated method when possible) */

    return this.locationRepository.findOneById(id);
  }

  create(newLocation: CreateLocation): Promise<Location> {
    return this.locationRepository.save(newLocation);
  }

  async update(id: string, updatedLocation: Location): Promise<Location> {
    return this.locationRepository.save(updatedLocation);
  }

  async delete(id: string): Promise<void> {
    const location = await this.getOne(id);

    await this.locationRepository.remove(location);
  }
}
