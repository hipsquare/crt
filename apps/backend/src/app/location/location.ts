import { Column, Entity, ObjectIdColumn } from 'typeorm';

export type CreateLocation = Omit<Location, 'id'>;

@Entity('locations')
export class Location {
  @ObjectIdColumn()
  id: string;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column()
  latitude: number;

  @Column()
  longitude: number;
}
