import { Test, TestingModule } from '@nestjs/testing';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthService } from './shared/auth/auth.service';
import { UsersService } from './shared/users/users.service';

describe('AppController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService, AuthService, UsersService],
    }).compile();
  });

  describe('getData', () => {
    it('should return "Hello API"', () => {
      const appController = app.get<AppController>(AppController);
      expect(appController.getData()).toEqual({ message: 'Hello API' });
    });
  });

  describe('getProtected', () => {
    it('should return "This is a protected resource. Welcome visitor!"', () => {
      const appController = app.get<AppController>(AppController);
      expect(appController.getProtected()).toEqual('This is a protected resource. Welcome visitor!');
    });
  });

  describe('getPublic', () => {
    it('should return "This is a public resource. Welcome member"', () => {
      const appController = app.get<AppController>(AppController);
      expect(appController.getPublic()).toEqual('This is a public resource. Welcome member');
    });
  });
});
