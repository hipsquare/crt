import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getData(): { message: string } {
    return { message: 'Hello API' };
  }

  getProtected(): string {
    return 'This is a protected resource. Welcome visitor!';
  }
  
  getPublic(): string {
    return 'This is a public resource. Welcome member';
  }
}
