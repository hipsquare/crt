import { Injectable } from '@nestjs/common';
import { User } from './users';

@Injectable()
export class UsersService {
    private readonly users: User[] = [];

    async findOne(username: string): Promise<User | undefined>{
        return this.users.find(user => user.username === username);
    }

    add(user: User): User{
        this.users.push(user);
        return user;
    }
}
