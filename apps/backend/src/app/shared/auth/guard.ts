import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common'; 
import { AuthService } from './auth.service';

@Injectable()
export class Guard implements CanActivate {
    constructor(private readonly authService: AuthService){}

  canActivate(
    context: ExecutionContext,
  ): boolean {
    const request = context.switchToHttp().getRequest();
    const cookie = request.headers.cookie;

    if (cookie && cookie === 'token=' + this.authService.token) {
      return true;
    }
    return false;
  }
}