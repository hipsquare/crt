import { Strategy } from 'passport-auth0';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from './auth.service';

@Injectable()
export class Auth0Strategy extends PassportStrategy(Strategy, 'auth0') {
  constructor(private authService: AuthService) {
    super({
      domain: process.env.AUTH_DOMAIN,
      clientID: process.env.AUTH_CLIENT,
      clientSecret: process.env.AUTH_SECRET,
      callbackURL: process.env.AUTH_CALLBACK,
      scope: 'openid email profile',
      state: false,
    });
  }

  // validate Auth0 user and return user information
  async validate(
    _accessToken: string,
    _refreshToken: string,
    profile: { displayName: string; user_id: string },
  ): Promise<any> {
    console.log('calling validate');
    // Call the AuthService to validate the Auth0 user
    const user = await this.authService.validateAuth0User({
      username: profile.displayName,
      id: profile.user_id,
    });

    // Log access token and profile (optional)
    console.log(_accessToken);
    console.log(profile);
    this.authService.token = _accessToken;

    this.authService.user = user;

    return user; // Return user information
  }
}
