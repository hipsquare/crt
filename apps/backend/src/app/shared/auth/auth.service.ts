import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { User } from '../users/users';

@Injectable()
export class AuthService {

    token?: string;
    user?: User;

    constructor(private usersService: UsersService){}

    async validateAuth0User({ username, id }: { username: string; id: string }) {
        let user = await this.usersService.findOne(username);

        if (!user) {
          // If user doesn't exist, add the user to the database
          user = this.usersService.add({
            password: '', // No password since it's an Auth0 user
            name: username,
            username: username,
            id: +id,
          });
        }
        const { password: _, ...result } = user; // Remove password from the returned user object
        return result; // Return user information excluding the password
      }
}
