import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { UsersService } from '../users/users.service';
import { Auth0Strategy } from './auth0.strategy';

@Module({
  imports: [PassportModule],
  providers: [AuthService, UsersService, Auth0Strategy],
  exports: [AuthService]
})
export class AuthModule {}
