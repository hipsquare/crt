import { Controller, Get, Redirect, Res, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './shared/auth/auth.service';
import { Response } from 'express';
import { Guard } from './shared/auth/guard';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, private readonly authService: AuthService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @Get('/public')
  getPublic(): string {
    return this.appService.getPublic();
  }

  @UseGuards(AuthGuard('auth0'))
  @Get('/protected')
  getProtected(): string {
    return this.appService.getProtected();
  }

  @Get('/logged-in-user')
  getLoggedInUser(): string  {
    return JSON.parse('{"email": "' +this.authService.user?.username + '"}');
  }

  @Get('/logout')
  @UseGuards(Guard)
  logout(@Res({ passthrough: true }) res: Response)  {
    this.authService.user = undefined;
    this.authService.token = undefined;
    res.cookie('token',undefined);
    res.redirect('https://schaukel.eu.auth0.com/oidc/logout');
  }

  @Get('auth0/login')
  @UseGuards(AuthGuard('auth0'))
  async auth0Login() {
    // No explicit implementation is needed here as AuthGuard handles the authentication flow
  }

  @Get('callback')
  @UseGuards(AuthGuard('auth0'))
  @Redirect('http://localhost:4200/home')
  async callback(@Res({ passthrough: true }) res: Response) {
    res.cookie('token',this.authService.token);
    return;
  }
}
