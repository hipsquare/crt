import { Test } from '@nestjs/testing';

import { AppService } from './app.service';
import { UsersService } from './shared/users/users.service';
import { AuthService } from './shared/auth/auth.service';

describe('AppService', () => {
  let service: AppService;

  beforeAll(async () => {
    const app = await Test.createTestingModule({
      providers: [AppService, UsersService, AuthService],
    }).compile();

    service = app.get<AppService>(AppService);
  });

  describe('getData', () => {
    it('should return "Hello API"', () => {
      expect(service.getData()).toEqual({ message: 'Hello API' });
    });
  });

  describe('getProtected', () => {
    it('should return "This is a protected resource. Welcome visitor!"', () => {
      expect(service.getProtected()).toEqual('This is a protected resource. Welcome visitor!');
    });
  });

  describe('getPublic', () => {
    it('should return "This is a public resource. Welcome member"', () => {
      expect(service.getPublic()).toEqual('This is a public resource. Welcome member');
    });
  });

});
